/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64;

/**
 *
 * @author ACER
 */
public class TestNovelKid {
    public static void main(String[] args) {
        NovelKid Noi = new NovelKid();
        Noi.accountName = "Noi";
        Noi.moneyBalance = 300.00; //น้อยมีงบในการซื้อ 300 บาท
        System.out.println("Noi Balance before purchase: " + Noi.getBalance()); //จำนวนเงินก่อนที่น้อยจะจ่ายค่าหนังสือ
        Noi.payBook(1000); //น้อยซื้อหนังสือราคา 1000 บาท
        System.out.println("Name: "+Noi.getName()+" Balance: "+ Noi.getBalance()); // จำนวนเงินปัจจุบันที่เหลือของน้อย
        Noi.payBook(250.50); //น้อยซื้อหนังสือราคา 250.50 บาท
        System.out.println("Noi Purchase the book.");
        System.out.println("Name: "+Noi.getName()+" Balance: "+ Noi.getBalance()); // จำนวนเงินปัจจุบันที่เหลือของน้อย
        Noi.payBook(530.00); //น้อยลองซื้อหนังสืออีกเล่มที่ราคาเกินงบ
        System.out.println("Noi Purchase the book.");
        System.out.println("Name: "+Noi.getName()+" Balance: "+ Noi.getBalance()); 
    }
}
