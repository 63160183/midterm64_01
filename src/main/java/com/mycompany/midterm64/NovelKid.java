/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64;
// ทดสอบข่อ 1.

import java.awt.BorderLayout;

/**
 *
 * @author ACER
 */
public class NovelKid {
    public String accountName;
    public double moneyBalance;

    public void payBook(double purchase){ //จ่ายค่าหนังสือ
        if(purchase > 300){ //ถ้าราคาเกินกว่า 300 บาท(งบของน้อย) จะซื้อไม่ได้
            System.out.println("You not have enought money to buy");
            return;
        }
        this.moneyBalance = this.moneyBalance - purchase; // งบเงิน - ค่าหนังสือที่ต้องจ่าย
    }
    public String getName(){ //เรียกใช้ตัวแปรชื่อ
        return accountName;
    }
    public double getBalance(){ //เรียกใช้ตัวแปรเงิน
        return moneyBalance;
    }
}
